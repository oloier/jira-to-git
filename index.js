const fetch = require("node-fetch")
const btoa = require("btoa")
const Promise = require("bluebird")
global.Headers = fetch.Headers

// account / project / access credentials
const JIRA_URL = "https://ucsadjusters.atlassian.net"
const JIRA_PROJECT = "UCS"
const JIRA_USERID = "soloier@ucsadjusters.com"
const JIRA_TOKEN = "EbV9PTdVsZUQ89Hn1IfeA500"
const GITLAB_URL = "https://gitlab.com/"
const GITLAB_PROJECT = 15232896
const GITLAB_TOKEN = "3ECNHq2EBxqQVibpDxEP"

// jira limits issues to 100 per-page. input number of expected pages
const maxResults = 100 
const jiraIssuesPageCount = 19 

const makeAPIRequest = async (options) => {
	const response = await fetch(options.url, {
		method: options.method,
		headers: options.headers,
		body: options.hasOwnProperty("body") ? JSON.stringify(options.body) : null,
	})

	// just some output to check status
	console.log(options.url)
	if (options.body) {
		console.log(options.body)
	}
	console.log(response.status)

	if (response.status >= 200 && response.status < 300) {
		return response.json()
	}
	else {
		throw `makeAPIRequest failed: ${response.status}`
	}
}

const getJiraIssues = async (startAt, maxResults) => {
	const fetchOpts = {
		url: `${JIRA_URL}/rest/api/2/search?jql=project=${JIRA_PROJECT}+order+by+id+asc&startAt=${startAt}&maxResults=${maxResults}`,
		method: "get",
		headers: new Headers({
			Authorization: "Basic " + btoa(`${JIRA_USERID}:${JIRA_TOKEN}`),
		}),
	}

	return makeAPIRequest(fetchOpts)
}

const getJiraIssuesComments = async (jiraIssueID) => {
	const fetchOpts = {
		url: `${JIRA_URL}/rest/api/2/issue/${jiraIssueID}/?fields=comment`,
		method: "get",
		headers: new Headers({
			Authorization: "Basic " + btoa(`${JIRA_USERID}:${JIRA_TOKEN}`),
		}),
	}

	return makeAPIRequest(fetchOpts)
}

const postGitlabIssue = async (gitlabIssue) => {
	const fetchOpts = {
		url: `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT}/issues`,
		method: "post",
		headers: new Headers({
			"PRIVATE-TOKEN": GITLAB_TOKEN,
			"Accept": "application/json",
			"Content-Type": "application/json",
		}),
		body: gitlabIssue,
	}

	return makeAPIRequest(fetchOpts)
}

const closeGitlabIssue = async (issueIID, updatedAt) => {
	const fetchOpts = {
		url: `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT}/issues/${issueIID}`,
		method: "put",
		headers: new Headers({
			"PRIVATE-TOKEN": GITLAB_TOKEN,
			"Accept": "application/json",
			"Content-Type": "application/json",
		}),
		body: {
			state_event: "close",
			update_at: dateToISO8601(updatedAt),
		},
	}

	return makeAPIRequest(fetchOpts)
}

const updateGitlabIssueComment = async (issueIID, commentData) => {
	const fetchOpts = {
		url: `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT}/issues/${issueIID}/notes`,
		method: "post",
		headers: new Headers({
			"PRIVATE-TOKEN": GITLAB_TOKEN,
			"Accept": "application/json",
			"Content-Type": "application/json",
		}),
		body: commentData,
	}

	return makeAPIRequest(fetchOpts)
}

const dateToISO8601 = (date) => {
	const newDate = new Date(date)
	return newDate.toISOString()
}

const executeRequests = async (startAt, maxResults) => {
	const jiraIssues = await getJiraIssues(startAt, maxResults)

	return Promise.map(
		jiraIssues.issues,
		async (jiraIssue) => {
			const jiraIssueComments = await getJiraIssuesComments(jiraIssue.id).catch(err =>
				console.log(err),
			)
			const jiraComments = jiraIssueComments.fields.comment.comments

			const gitlabIssue = {
				title: `${jiraIssue.key} – ${jiraIssue.fields.summary}`,
				// iid: jiraIssue.key, // apparently being the sole maintainer is not owner/admin in gitlab
				description: jiraIssue.fields.description,
				labels: [jiraIssue.fields.issuetype.name.toLowerCase()],
				created_at: dateToISO8601(jiraIssue.fields.created),
				updated_at: dateToISO8601(jiraIssue.fields.updated),
				assignee_ids: [2284730],
				reporter: null,
			}

			const newGitlabIssue = await postGitlabIssue(gitlabIssue).catch(err => console.log(err))
			console.log(newGitlabIssue)
			if (jiraIssue.fields.status.statusCategory.name === "Done") {
				await closeGitlabIssue(newGitlabIssue.iid, jiraIssue.fields.updated).catch(err =>
					console.log(err),
				)
			}

			for (const comment of jiraComments) {
				const gitlabNote = {
					body: comment.body,
					created_at: dateToISO8601(comment.created),
					updated_at: dateToISO8601(comment.updated),
				}
				await updateGitlabIssueComment(newGitlabIssue.iid, gitlabNote).catch(err =>
					console.log(err),
				)
			}

			// since we can't close the issue when it originally was, lets add a new comment noting this
			const closeDate = new Date(jiraIssue.fields.updated)
			const closeNote = {
				body: `Original issue was closed on ${closeDate.toString()}`,
				created_at: dateToISO8601(jiraIssue.fields.updated),
				updated_at: dateToISO8601(jiraIssue.fields.updated),
			}
			await updateGitlabIssueComment(newGitlabIssue.iid, closeNote).catch(err =>
				console.log(err),
			)
		},
		{concurrency: 6},
	)
}

async function executeWithOffset(offsetCount) {
	if (offsetCount > jiraIssuesPageCount) {
		return
	}
    
	const startAt = offsetCount * maxResults

	try {
		await executeRequests(startAt, maxResults)
	}
	catch (error) {
		console.error(error)
	}

	return executeWithOffset(offsetCount += 1)
}

executeWithOffset(0)
